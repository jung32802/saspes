# SAS Powerschool Enhancement Suite

SAS PES is an extension using the WebExtension standard for use by Singapore American School students. The extension provides various enhancements when using Powerschool at SAS.

## Features

### Main Page
* View final percent for each course
* View current semester GPA
* Recalculate semester GPA with custom hypothetical grades.

### Class Page
* View final percent and cutoffs for grades

### Extra Features
* (Temporarily removed for development) ~~View grades and final percents for courses even when Powerschool is closed.~~

## Browser Support

### Offically Supported

* Firefox
* Chromium

Please be aware that the extension is mainly designed for use in Firefox.

### Unoffical Support

People have gotten the extension to work on the following browsers

* Opera
* Vivaldi

## Download

Downloads for both Firefox and Chromium can be found on the project website: [https://gschool.ydgkim.com/saspowerschool/](https://gschool.ydgkim.com/saspowerschool/?pk_campaign=SASPES&pk_kwd=gitlab-readme)

## 3rd Party Libraries

* jQuery
* Mozilla WebExtension Browser API Polyfill
    * Used only in Chromium version

Refer to the 'Third-Party Licenses.txt' file for their respective licenses.

## License

Copyright &copy; 2018-2019 Gary Kim (gary@ydgkim.com)

Source code licensed under GPL-3.0.

The logo and name are not licensed for use, modification, or distribution.